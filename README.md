## test images 

test images with one person per image : images/test

test images with **4** people per image : 4people-test-images

Name of the image file is the ground truth. 

## training images 

training images: images/train

128 descriptors for training images : features/reps.csv

corresponding labels for 128 descriptors for training images : features/labels.csv

linear svm trained using 128 descriptors : features/classifier.pkl

number of people in training set : 16

## download from: https://bitbucket.org/sidgan/demo/downloads/



instructions to run code; https://gist.github.com/sidgan/5ce4bb798f6871f380cbe5bc1d30c5d2
